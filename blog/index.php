<!DOCTYPE html>
<html>
<head>
<title>Aviation School</title>
<meta charset="UTF-8" />

      <link rel="stylesheet" type="text/css" href="styles/style.css" />
      <link href="styles/style1.css" rel="stylesheet" type="text/css">
      <link href="styles/style2.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="styles/ddsmoothmenu.css">
      <link href="Popup_Style.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="styles/jquery.fancybox-1.3.4.css" media="screen">
      <link href="styles/style1.css" rel="stylesheet" type="text/css">
      <script src="js/jssor.slider-26.5.2.min.js" type="text/javascript"></script>
      <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideWidth: 720,
              $Cols: 2,
              $Align: 130,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb051 .i {position:absolute;cursor:pointer;}
        .jssorb051 .i .b {fill:#fff;fill-opacity:0.5;}
        .jssorb051 .i:hover .b {fill-opacity:.7;}
        .jssorb051 .iav .b {fill-opacity: 1;}
        .jssorb051 .i.idn {opacity:.3;}

        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
    </style>
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="styles/ie6.css" /><![endif]-->
</head>
<body style="background: linear-gradient(to bottom right,white,#bcbdc4);">
  
<div class="mid">
  <div id="header">
    <div id="section">
      <div><a href="index.html"><img src="images/logo.png" alt="" /></a></div>
      <span> <br />
      <br />
      </span>
       </div>
     <ul>
      <li class="current"><a href="index.php">Home</a></li>
      <li><a href="about.php">About Us</a></li>
      <li><a href="#">Academics</a></li>
      <li><a href="#">Admissions</a></li>
      <li><a href="#">Research</a></li>
      <li><a href="#">Faculties</a></li>
      <li><a href="login.php">Login</a></li>
      <li><a href="#">Colleges</a></li>
    </ul>

    <!-- slider -->
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
            <div>
                <img data-u="image" src="img/001.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/002.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/003.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/004.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/005.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/006.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/007.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/009.jpg" />
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:35px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:35px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    <!-- slider end -->
     <div class="content_links" style="padding: 0;">
                     <ul>
                        <li><a href="das/index.html" target="_parent" class="our_university">DASR</a></li>
                        <li><a href="qec/index.html" target="_parent" class="admission">QEC</a></li>
                        <li><a href="http://sau.edu.pk/c&amp;n/" target="_parent" class="accommodaiton">C&amp;N Section</a></li>
                        <li><a href="pro/index.html" target="_parent" class="community">PRO Section </a></li>
                        <li><a href="sau-scholarships-all.php" target="_parent" class="schorship">Scholorships</a></li>
                        <li class="last"><a class="take_tour" href="#">Take a Tour</a></li>
                     </ul>
                  </div>
  </div>
  <div id="content">

    <div id="home">

      <div>
        <h1><marquee align="center" >Welcome to <span >Sindh Agriculture University,Tandojam</span></marquee></h1>
        <div id="aside">
          <div>
            
            <div>
              <div style="background-color:#e7f1ff;"><p>Message from vice chanceleor</p></div>
            <a href="#"><img style="width:150px;" src="images/Dr.-Mujeeb-Sahari.JPG" alt="" /></a>
            
            <h5>Message from vice chanceleor</h5>
              <p >Agriculture has a vital role in the economy of Pakistan and it is the essential source of food, income and employment. Agriculture contributes around 21% to the Gross Domestic Production (GDP), employs nearly 45% of country's labour force and actively contributes in the growth of other sectors of economy.
               However, it is pertinent to mention here that the referred 21% GDP has declined steadily from 45% during the previous several years.</p>
               </div>
          </div>
       
          <div>
            <h3 style="background-color:#e7f1ff; height:20px; font-size:20px;"> <a href="#">Current News &amp; Events Updates </a> </h3>
            <div class="degree_type2">
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/admissions-2017-2018/admission-notice-2017-2018.jpg" target="_parent">Undergraduate Admission Announcement for Session 2017-2018</a> - <img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/images/Image-(35).jpg" target="_parent">SAU (Interfaculty) Football Championship 2017-2018</a> - <img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/images/Image-(34).jpg" target="_parent">2nd SAU Girls Badminton Championship 2017-18</a> - <img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p>HEC formally declares establishment of ORIC at Sindh Agriculture University, Tandojam. <a style="font-weight: bold; color: #333" a="" href="http://sau.edu.pk/downloads/SAU-Tandojam-ORIC-Notification.jpg" target="_parent">HEC Notification</a>  <img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/downloads/Advertisement_UK.pdf" target="_parent">Employment Opportunity at Sindh Agriculture University, Umerkoat Sub-Campus</a> - <img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/downloads/convocation-10-10-17.jpg" target="_parent">11th Convocation 2017 Announcement</a> - <a style="" a="" href="http://sau.edu.pk/downloads/convocation-application-form.docx"><span style="color: #333; font-weight: bold;">Click here to Download Form</span></a><img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/downloads/Revised_advert-sagp.pdf" target="_parent">Applications invited under Research Project entitled “Seedling and Root Diseases of Chilli and their Control” </a><img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/downloads/tender/Tender-SAU-and-ZABAC-29-08-2017.pdf" target="_parent">Tender notice for the supply of Desktop Computers, Printers and Multimedia Projectors </a><img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     <p><a style="text-decoration:none" a="" href="http://sau.edu.pk/downloads/engro-frt-advt.jpg" target="_parent">Employment Opportunity at Engro Fertilizer Company for Field Officer</a><img src="images/New_Blink_3.gif" width="25" height="9"> </p>
                     
                     
                  </div>
          </div>

        </div>
        <div id="calendar">
          <h3>Fast Links</h3>
          <ul>
            <li>
              <div> <span><a href="#"><img src="images/student1.jpg" alt=""></a><br />
                </span>
                <h2><a href="#">Quality Enhancement Cell</a></h2>
              </div>
              <p>Quality Enhancement Cell has been established at Sindh Agriculture University, Tandojam in 2006 in second phase of QECs established by Higher Education Commission.</p>
            </li>
            <li>
              <div> <span><a href="#"><img src="./images/student2.jpg" alt=""></a><br />
                </span>
                <h2><a href="#">SAU jobs</a></h2>
              </div>
              <p>Vacancies of Teaching and Non Teaching staff at Umerkoat Sub-campus (27-October-2017),Vacancies of Teaching -KCAET. (23-August-2017)Vacancies of Teaching and Non Teaching Staff (13-August-2017)</p>
            </li>
            <li>
              <div> <span><a href="https://outlook.office365.com/"><img src="images/student4.jpg" alt=""></a><br />
                </span>
                <h2><a href="#">Digital Library</a></h2>
              </div>
              <p>If you're having problems editing this website template, then don't hesitate to ask for help on the Forum.</p>
            </li>
             <li>
              <div> <span><a href="https://outlook.office365.com/"><img src="images/student9.jpg" alt=""></a><br />
                </span>
                <h2><a href="#">SAU Email.</a></h2>
              </div>
              <p>If you're having problems editing this website template, then don't hesitate to ask for help on the Forum.</p>
            </li>
          </ul>
        </div>
        <div>
      
 
     
        </div>
        <hr>

    
            <div style="background-color:black;width:824px;margin-bottom:0px;">
              
                   <p style="text-align:center;color:white;">© 2007-2016, Sindh Agriculture University, Tandojam. All rights reserved
      </div>
  </div>



    <script src="js/ddsmoothmenu.js" type="text/javascript"></script>
     
         
<script type="text/javascript">jssor_1_slider_init();</script>
<style type="text/css">

</style>
</body>
</html>