<!DOCTYPE html>
<html>
<head>
<title>Aviation School</title>
<meta charset="UTF-8" />

      <link rel="stylesheet" type="text/css" href="styles/style.css" />
      <link href="styles/style1.css" rel="stylesheet" type="text/css">
      <link href="styles/style2.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="styles/ddsmoothmenu.css">
      <link href="Popup_Style.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="styles/jquery.fancybox-1.3.4.css" media="screen">
      <link href="styles/style1.css" rel="stylesheet" type="text/css">
      <script src="js/jssor.slider-26.5.2.min.js" type="text/javascript"></script>
      <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideWidth: 720,
              $Cols: 2,
              $Align: 130,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb051 .i {position:absolute;cursor:pointer;}
        .jssorb051 .i .b {fill:#fff;fill-opacity:0.5;}
        .jssorb051 .i:hover .b {fill-opacity:.7;}
        .jssorb051 .iav .b {fill-opacity: 1;}
        .jssorb051 .i.idn {opacity:.3;}

        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
    </style>
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="styles/ie6.css" /><![endif]-->
</head>
<body style="background: linear-gradient(to bottom right,white,#bcbdc4);">
  
<div class="mid">
  <div id="header">
    <div id="section">
      <div><a href="index.html"><img src="images/logo.png" alt="" /></a></div>
      <span>aviationschool@flying.com <br />
      <br />
      + 7958 917 9747 2481 000</span>
       </div>
    <ul>
      <li class="current"><a href="index.php">Home</a></li>
      <li><a href="about.php">About Us</a></li>
      <li><a href="#">Academics</a></li>
      <li><a href="#">Admissions</a></li>
      <li><a href="#">Research</a></li>
      <li><a href="#">Faculties</a></li>
      <li><a href="login.php">Login</a></li>
      <li><a href="#">Colleges</a></li>
    </ul>

    <!-- slider -->
    
    <!-- slider end -->
    
  </div>
  
       <div class="container">
        <div class="row">
          <div class="col-md-9">
            <h1>About Us</h1>
            <br><br>
            <p>Sindh inherits the fertile Indus Valley, which is well known for its glorious past. The excavation of Moen-Jo-Daro talks about prosperity of Sindh for the production of agricultural commodities in earliest times. Sindh province of Pakistan was a net exporter of food supplies to the entire Sub-continent during the nineteenth century. Located in the extreme south of Pakistan as one of the four provinces, Sindh presents a variety of soil and climatic conditions compatible for growing various cereal, fruit and vegetable crops and maintenance of a large livestock population.</p>
            <br><br>
            <p>After the commissioning of the Sukkur Barrage in 1932, the need for an institution of agricultural education exclusively for Sindh was felt formidable. Thus, an Agriculture College associated with Agriculture Research Institute was established at Sakrand in District Nawabshah in 1939-40, and named as the King George-V Institute of Agriculture. With the advent of Kotri barrage in 1955, the Government of Sindh under a comprehensive plan shifted the college to Tando Jam in 1955 with added facilities of land, buildings and staff. Since then, it started flourishing. Being the only agricultural college in whole of Southern region of Pakistan. It catered to the needs of trained manpower in agriculture for Sindh and Balochistan provinces of Pakistan. The entire country is bestowed by Almighty Allah with tremendous natural resources where agriculture plays a vital role in country’s development. Agriculture contributes to 23.3% of the overall GDP and employs almost half of the labor force of the country. The livestock alone contributes half of the total contribution of agriculture to the GDP. A total of 30 to 35 million rural people are engaged in Agriculture.</p>
            <br><br>
            <p>
              At Tando Jam, the college was able to promote agricultural education steadily in the province. Thus remarkable accomplishments of the college paved the way for raising the status of the college to the full-fledged university on 1st March, 1977, named as Sindh Agriculture University, Tando Jam established under Act of 1977. The broad objectives of establishing Agriculture University, Tando Jam were to sharpen focus on agricultural education, improve quality of learning, promote integrated outlook in the functions of teaching, research and extension, produce service-oriented graduates and generate appropriate technologies.
            </p>
            <br><br>
            <p>Pakistan is one of the signatory countries of World Trade Organization (WTO). The agreements on Agriculture in the context of free trade world wide have increased the responsibilities of agricultural universities of the country to groom and motivate the youth specializing in different disciplines of agriculture by strengthening their efforts manifold to face the competition in agriculture trade and services world wide. The university faculty has to formulate a road map to combat the challenges that our farmers are anticipating under WTO regime. We are extremely conscious of our role in fulfilling these challenges.</p>
            <br><br>
          </div>
        </div>
       </div>
        <hr>

    
            <div style="    background-color: black;
            text-align:center;
    width: 960px;
    margin-top:5px;
    padding:top:5px;
    margin-bottom: 0px;
    height: 50px;">
              
                   <p style="text-align:center;color:white;">© 2007-2016, Sindh Agriculture University, Tandojam. All rights reserved
      </div>
  </div>



    <script src="js/ddsmoothmenu.js" type="text/javascript"></script>
     
         
<script type="text/javascript">jssor_1_slider_init();</script>
<style type="text/css">

</style>
</body>
</html>